# mzphp 用户系统

您可以用本代码配合 [mzphp](http://git.oschina.net/mz/mzphp2) 快速实现用户系统。

# 安装使用

请将 mzphp 框架代码放至与本项目同级。

1. 编辑conf/conf.xxx.php基本配置，并保证配置的 mysql 表，database 库名存在。
2. 打开命令行，执行 php index.php install init 进行数据库初始化。
3. 访问首页。

# 编辑或者拓展表结构

1. 用工具修改表结构或者修改control/install_control.class.php再运行。
2. 修改core/user.class.php 中 user 类的 $tables 对应的表和字段来修改用户基础信息结构。

# user 方法

user类直接在control类中使用自动加载的方式：

```
class index_control extends auth_contorl{
    public function on_index(){
        // 登录uid为1的用户
        $this->user->login(1);
        // 获得当前登录用户的user_id
        $uid = $this->user->user_id;
        // 获得当前登录用户的所有字段
        $user = $this->user->field();
        // 获得当前登录用户的 logcount 字段
        $login_count = $this->user->field('logcount');
        // 获得用户uid=9 的用户 phone 字段
        $phone = $this->user->field('phone');
        // 退出登录
        $this->user->logout();
        // 注册流程
        // 生成盐
        $salt = $this->user->get_salt();
        // 生成密码
        $pass = $this->user->get_pass('admin888', $salt);
        // 注册用户
        $uid  = $this->user->register('admin', '18888888888', $pass, $salt, 1);

    }
}
```


# 更多细节

具体请参考源码 core/user.class.php