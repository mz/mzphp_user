<?php
!defined('FRAMEWORK_PATH') && exit('Access Deined.');

/**
 * Class index_control
 * 初始化数据库和管理员
 * 配置好conf目录文件后 确保 database 存在，开启memcache，执行
 * php index.php install init
 */
class install_control extends cli_control {

    private $admin_username = 'admin';
    private $admin_password = 'admin888';
    private $admin_phone = '18888888888';

    /**
     * @param $conf
     */
    function __construct(&$conf) {
        // 继承 auth_control 的析构
        parent::__construct($conf);
    }

    /**
     * 登录后的首页
     */
    public function on_init() {
        $sqls = "
DROP TABLE IF EXISTS pre_user;
CREATE TABLE `pre_user` (
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户uid',
  `username` CHAR(30) NOT NULL DEFAULT '' COMMENT '用户名',
  `phone` CHAR(11) NOT NULL DEFAULT '' COMMENT '手机',
  `password` CHAR(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` CHAR(8) NOT NULL DEFAULT '' COMMENT '密钥',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `phoneIndex` (`phone`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COMMENT='用户登录表';
DROP TABLE IF EXISTS pre_user_field;
CREATE TABLE `pre_user_field` (
  `user_id` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '用户uid',
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '状态0未激活1正常',
  `isadmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '1普通管理员2高级管理员',
  `phonecheck` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否手机激活',
  `regtime` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '注册时间',
  `regip` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '注册ip',
  `logtime` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `logip` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `logcount` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '登录次数',
  PRIMARY KEY (`user_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 COMMENT='用户拓展信息表';
";
        $sqls = explode(';', $sqls);
        foreach ($sqls as $sql) {
            if(!trim($sql)){
                continue;
            }
            $sql = str_replace('pre_', $this->conf['db']['mysql']['tablepre'], $sql);
            DB::query($sql);
        }

        $salt = $this->user->get_salt();
        $pass = $this->user->get_pass($this->admin_password, $salt);
        $uid  = $this->user->register($this->admin_username, $this->admin_phone, $pass, $salt, 1);
        log::info('install success', 'adminID=' . $uid);
    }

}

?>