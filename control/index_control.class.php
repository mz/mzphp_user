<?php
!defined('FRAMEWORK_PATH') && exit('Access Deined.');

/**
 * Class index_control
 * 继续 auth_control 就会检测用户是否登录
 */
class index_control extends auth_control {

    function __construct(&$conf) {
        // 当 action 为 test 开启不验证模式
        if (in_array(core::R('a'), array('test'))) {
            define('NO_AUTH', 1);
        }
        // 继承 auth_control 的析构
        parent::__construct($conf);
    }

    /**
     * 登录后的首页
     */
    public function on_index() {
        // 取当前用户信息
        $user = $this->user->field('');
        // 绑定模板变量
        VI::assign('user', $user);
        $this->show('index');
    }

    /**
     * 继承 on_login 并显示
     */
    public function on_login() {
        // 绑定模板变量
        VI::assign_value('message', '查看首页，请登录');
        // 登录
        parent::on_login();
    }

    /**
     * 测试
     */
    public function on_test() {
        // jsonp
        $this->show_json('这是不需要登录的内容', 0, 0, 1);
    }
}

?>