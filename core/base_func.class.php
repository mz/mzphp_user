<?php
!defined('ROOT_PATH') && exit('Access Denied');

/**
 * ft text for spacing
 *
 * @param       $image
 * @param       $font_size
 * @param       $text_angle
 * @param       $start_x
 * @param       $start_y
 * @param       $color
 * @param       $font_file
 * @param       $text
 * @param array $extra_info
 * @return array
 */
function imagefttext2($image, $font_size, $text_angle, $start_x, $start_y, $color, $font_file, $text, $extra_info = array()) {
    if ($extra_info['character_spacing'] == NULL || !is_numeric($extra_info['character_spacing'])) {
        $extra_info['character_spacing'] = 0;
    }
    $lastX       = $start_x - $extra_info['character_spacing'];
    $coordinates = array();
    foreach (str_split($text) as $word) {
        $angle       = $text_angle ? rand(-$text_angle, $text_angle) : 0;
        $coordinates = imagefttext($image, $font_size, $angle, $lastX + $extra_info['character_spacing'], $start_y, $color, $font_file, $word, $extra_info);
        $lastX       = max($coordinates[2], $coordinates[4]);
    }
    // Return the newly generated image box coordinates:
    return array($start_x, $start_y, $coordinates[2], $coordinates[3], $coordinates[4], $coordinates[5], $start_x, $coordinates[7]);
}

/**
 * @param        $text
 * @param        $image_width
 * @param        $image_height
 * @param string $font_file
 * @param int    $output
 * @return bool
 */
function checkcode_image($font_file = 'fonts/checkcode.ttf', $output = 1) {
    $actions = array('+', '-', ''/*, '÷', '×'*/);
    $action  = $actions[array_rand($actions)];
    $number1 = rand(0, 99);
    while ($action == '-' && $number1 == 0) {
        $number1 = rand(0, 99);
    }
    $number2 = $action == '-' ? rand(0, $number1) : rand(0, 99);
    if ($action == '') {
        $text = $number1 . $number2;
    } else {
        $text = $number1 . $action . $number2 . '=';
    }
    $result = 0;
    switch ($action) {
        case '+':
            $result = $number1 + $number2;
        break;
        case '-':
            $result = $number1 - $number2;
        break;
        case '÷':
            $result = $number1 / $number2;
        break;
        case '×':
            $result = $number1 * $number2;
        break;
        case '':
            $result = intval($text);
        break;
    }

    $image_width  = strlen($text) * 25;
    $image_height = 16;
    //建立图象
    $image = imagecreate($image_width, $image_height);
    if (!$image) {
        return false;
    }
    imagesavealpha($image, true);
    imagealphablending($image, false);
    // 图片色彩设置
    $background_color = imagecolorallocate($image, 255, 255, 255);
    // 透明颜色
    imagecolortransparent($image, $background_color);
    // 加入干扰因素
    for ($i = 0; $i < 0; $i++) {
        $rand_color = imagecolorallocate($image, rand(0, 255) + 100, rand(0, 255) + 100, rand(0, 255) + 110);
        $x1         = rand(0, $image_width);
        $y1         = rand(0, $image_height);
        for ($j = 0; $j < 1; $j++) {
            $x2 = min($x1 + $image_width + $j, $image_width);
            $y2 = min($y1 + $image_height + $j, $image_height);
            imageline($image, $x1 - $j, $y1 - $j, $x2, $y2, $rand_color);
        }
    }
    $text_color = imagecolorallocate($image, 0, 14, 5);
    imagefttext2($image, 16, 5, 5, 16, $text_color, core::$conf['static_dir'] . $font_file, $text, array('character_spacing' => 2));

    //建立png图型
    header("Content-type:image/png");
    core::C('checkcode_ip', core::ip(), 3600);
    //checkcode for ip
    CACHE::set('checkcode_' . core::ip(), $result, 3600);
    imagepng($image, null, 9);
    imagedestroy($image);
}

/**
 * check code valid
 *
 * @param $code
 * @return bool
 */
function checkcode_valid($code) {
    $ip     = core::C('checkcode_ip') ? core::C('checkcode_ip') : core::ip();
    $result = CACHE::get('checkcode_' . $ip);
    CACHE::delete('checkcode_' . $ip);
    core::C('checkcode_ip', '', 0);
    header('code:' . $result);
    if ($code == $result) {
        return true;
    } else {
        return false;
    }
}

?>