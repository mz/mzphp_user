<?php
!defined('ROOT_PATH') && exit('Access Denied');

/**
 * Class auth_control
 * 用户验证基类
 */
class auth_control extends base_www {
    /**
     * 析构方法：
     * 当继承本类时，所有的子类方法都需要登录才能显示具体内容
     * 可以在子类调用父类析构前，定义 NO_AUTH 表示接口不需要检测用户
     * define('NO_AUTH', 1);
     * 同时，检测方法有两种，
     * 1. 读取 cookie 中的 auth 数据和数据库中的做对比（默认，相对安全、效率低）
     * 2. 读取 cookie 中的 auth 做简单的对比（相对弱安全，效率高）
     * 可以通过定义
     * define('AUTH_SIMPLE', 1);
     * 开启2，简单模式来验证用刻苦     * 开启2，简单模式来验证用户 auth
     *
     * @param $conf
     */
    function __construct(&$conf) {
        parent::__construct($conf);

        // not login action
        $no_login = array('register', 'logout', 'agreement', 'forgot', 'login', 'checkcode');
        if (in_array(core::R('a'), $no_login)) {
            return;
        }

        // is defined NO_AUTH or not has user_id
        if (!defined('NO_AUTH') && !$this->user->user_id) {
            $this->on_login();
            exit;
        }
    }

    /**
     * get check code
     */
    function on_checkcode() {
        checkcode_image();
    }

    /**
     * checkcode valid
     */
    function checkcode_valid() {
        $code = core::P('CHECK_CODE');
        $res  = 0;
        if ($code) {
            $res = checkcode_valid($code);
        }
        if (!$res) {
            $this->show_json('验证码错误', 99);
        }
    }


    /**
     * 登录
     */
    function on_login() {
        if (misc::form_submit()) {
            $this->_login();
        }
        $this->show('user/login');
    }


    /**
     * login
     */
    function _login() {
        $user_model = $this->user;
        $password   = core::P('password');
        if ($message = $user_model->check_password($password)) {
            $this->show_json($message);
        }
        $this->checkcode_valid();

        $username = core::P('username:tel');
        // mobile phone length is 11
        if (strlen($username) == 11 && is_numeric($username) && substr($username, 0, 1) == '1') {
            $log_user = $user_model->get_by_phone($username);
        } else {
            $username = core::P('username');
            $log_user = $user_model->get_by_username($username);
        }

        if (!$log_user) {
            $this->show_json('用户不存在，请重试');
        }
        // get pass
        $pass = $user_model->get_pass($password, $log_user['salt']);
        // check password
        if ($pass && $log_user['password'] == $pass) {
            // get auth cookie
            $auth = $user_model->login($log_user['uid'], $pass);
            // get user info
            $user_info = array('uid'      => $log_user['uid'],
                               'username' => $log_user['username'],
                               'phone'    => $log_user['phone'],
                               'auth'     => $auth,
            );
            $this->show_json(1, 0, $user_info);
        } else {
            $this->show_json('登录失败，请重试');
        }
    }

    /**
     * logout
     */
    function _logout() {
        $this->user->logout();
    }

    /**
     * 是否管理员
     *
     * @return mixed
     */
    function is_admin() {
        return $this->user->field('isadmin');
    }

    /**
     * 绑定用户变量
     */
    public function assign_user($uid = 0) {
        VI::assign_value('user', $this->user->field('', $uid));
    }

    /**
     * 忘记密码
     */
    function on_forgot() {
        $this->show('user/forgot');
    }

    /**
     * 注册
     */
    function on_register() {
        $this->show('user/register');
    }

    /**
     * 协议
     */
    function on_agreement() {
        $this->show('user/agreement');
    }

    /**
     * 欢迎页面
     */
    function on_welcome() {
        $this->show('user/welcome');
    }

    /**
     * 退出
     */
    function on_logout() {
        if (misc::form_submit()) {
            $this->_logout();
        }
        $this->show('user/logout');
    }

}

?>